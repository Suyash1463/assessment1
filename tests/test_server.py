# test_server.py

import unittest
from server import app

class TestServer(unittest.TestCase):
    def test_index_route(self):
        tester = app.test_client(self)
        response = tester.get('/')
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()
