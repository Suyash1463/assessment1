Project README
This repository contains a Flask web application that serves a simple form and displays the submitted information on a separate page.

Prerequisites
Before running this application, ensure that you have the following installed:

Python 3.9
Git
Docker (if running in a Docker container)
Setup Instructions
Follow these steps to set up and run the application:

Clone the repository to your local machine:


git clone <repository_url>
Navigate to the project directory:


cd assessment1
(Optional) Set up a virtual environment (recommended):


python3 -m venv venv
source venv/bin/activate
Install the required dependencies:

bash
Copy code
pip install -r requirements.txt
Run the Flask application:

bash
Copy code
python server.py


Continuous Integration and Deployment
This project is set up with GitLab CI/CD for automated testing, building, and deployment. The .gitlab-ci.yml file defines the CI/CD pipeline with two stages: build and deploy.

In the build stage, the pipeline installs the project dependencies and runs any necessary build tasks.

In the deploy stage, the pipeline installs Gunicorn and Flask inside a virtual environment and starts the application using Gunicorn. This stage is triggered only when changes are pushed to the master branch.

Troubleshooting
If you encounter any errors related to missing modules or dependencies, ensure that you have installed all the required packages and activated the virtual environment (if using one).
Check the logs from the CI/CD pipeline for any error messages that might indicate issues with the deployment process.
Contributing

